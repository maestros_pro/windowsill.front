// Event listenr



$(function(){

	var timer = setInterval(function () {
		if ($.app === undefined) return false;
		clearInterval(timer);
		setTimeout(function () {
			eventLoader($.app)
		}, 0);
	}, 50);

	function eventLoader(self) {

	$('body')

		.on('submit', '.js-form', function (e) {
			e.preventDefault();
			var $form = $(this);


			if (self.checkForm($form)) {
				var data = $form.serialize() + '&' + $.param(self.fileUploader.files);

				console.info(data);
				$.ajax({
					type: $form.attr('method'),
					url: $form.attr('action'),
					data: data,
					complete: function (res) {

						var data = JSON.parse(res.responseText);

						if (res.status) {
							var status = res.status,
								formName = $form.data('form-name');

							if (status == 200) {    /** success */

								switch (formName) {
									case 'files':
										self.fileUploader.files = {};
										break;
									case 'feedback':

										break;

									case 'faq':

										break;
								}

								location.reload();

							} else if (status == 400) {    /** error */

							} else if (status == 500) {    /** Internal Server Error */

							} else {
								/** other trouble */
								console.error(res);
							}

						}
					}
				});
			}

		})

		.on('click', '.js-add-prod-in-cart', function(e){
			e.preventDefault();
			var $t = $(this),
				$wrap = $t.closest('.cost'),
				$field = $wrap.find('.cost__field'),
				fail = false;

			$field.removeClass('is-error');

			$field.each(function(){
				var $t = $(this);
				if (
					$t.hasClass('is-clean') ||
					(
						$t.hasClass('cost__field_input') &&
						( !$t.find('input').val() || $t.find('input').val() == '0' )
					)
				){
					fail = true;
					$t.addClass('is-error');
				}
			});

			if ( fail ) return false;

			/** TODO
			 * ------
			 */

		})

		.on('click', '.js-clear-all-prods', function(e){
			e.preventDefault();

			self.shadow.hide();

			$('.order__item').remove();
			$('.order__legend-val').text('0 руб.');
		})

		.on('click', '.hint__label, .hint__close', function(e){
			e.preventDefault();
			var $t = $(this),
				$wrap = $t.closest('.hint'),
				$text = $wrap.find('.hint__text');

			$wrap.toggleClass('is-open');
		})

		.on('click', '.product__view-item', function(e){
			e.preventDefault();

			var $t = $(this),
				$wrap = $t.closest('.product__inner'),
				src = $t.attr('data-src'),
				id = $t.attr('data-id'),
				label = $t.attr('data-label') || '',
				$label = $wrap.find('.product__view').find('.product__view-label'),
				$slider = $wrap.find('.product__view').find('.js-product-preview'),
				$material = $wrap.find('.product__info').find('.material__item'),
				$view = $wrap.find('.product__view').find('.product__view-img').find('img');

			$view.attr('src', src);

			$t.addClass('is-active').siblings().removeClass('is-active');
			$label.html(label);


			$material.filter('.is-active').removeClass('is-active');
			$material.filter(function (){ return $(this).attr('data-id') == id; }).eq(0).addClass('is-active');


			$slider.slick('slickGoTo', $t.index());
		})

		.on('click', '.product .material__item', function(e){
			e.preventDefault();

			var $t = $(this),
				$wrap = $t.closest('.product__inner') ,
				src = $t.attr('data-src'),
				id = $t.attr('data-id'),
				$material = $wrap.find('.product__info').find('.material__item'),
				$view = $wrap.find('.product__view').find('.product__view-item');

			$view.filter(function (){ return $(this).attr('data-id') === id; }).click();
			$material.filter('.is-active').removeClass('is-active');
			$t.addClass('is-active');

		})

		.on('click', '.examples .material__item', function(e){
			e.preventDefault();

			var $t = $(this),
				$wrap = $t.closest('.examples') ,
				bunch = $t.attr('data-bunch'),
				$item = $wrap.find('.examples__gallery-item[data-bunch=' + bunch + ']'),
				$material = $wrap.find('.material__item');

			$material.filter('.is-active').removeClass('is-active');
			$t.addClass('is-active');

			if ($item.length) $('.examples__gallery-list').mCustomScrollbar("scrollTo", $item.position().left);

		})




	;



		self.scrollBar.init('.examples__gallery-list', {
			axis:"x",
			theme:"light-3",
			advanced:{autoExpandHorizontalScroll:true}
		});


	}

});