(function(){
	var app = {
		module: {

			setMenuPosition: {

				pos: function ($el) {
					$el.scrollToFixed({
						marginTop: 0,
						zIndex: 99,
						removeOffsets: true
					});
				},

				init: function (el) {
					var $el = $(el);
					self.setMenuPosition.pos($el);
					$(window).resize(function () {
						$el.trigger('detach.ScrollToFixed');
						self.setMenuPosition.pos($el);
					});
				}
			},

			fileUploader: {
				isAdvancedUpload: function(){
					var div = document.createElement( 'div' );
					return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
				},


				count: 0,

				files: {},

				init: function () {

//http://iantonov.me/page/html5-na-primerah


					$('body')
						.on('click', '.js-uploaded-file-remove', function () {
							var $t = $(this),
								$field = $t.closest('.form__field_file');

							$field.remove();

							delete self.fileUploader.files[$field.data('id')];


							if ( $.isEmptyObject($.app.fileUploader.files) ) $('.js-file-noupload-info').show();


							$('#uploadFile')[0].value = null;
						})
					;

					if ( self.fileUploader.isAdvancedUpload ){
						var popup = $('.popup_installation'),
							dragBox = $('.dragbox');

						popup
							.on('dragenter', function (e) {
								e.stopPropagation();
								e.preventDefault();
								dragBox.show();
							})
							.on('dragover', function (e) {
								e.stopPropagation();
								e.preventDefault();
								dragBox.show();
							})
							.on('dragleave', function (e) {
								e.stopPropagation();
								e.preventDefault();
								if ( $(e.target).closest('.popup_installation .popup__layout').length < 1 ){
									dragBox.hide();
								}
							})
						;


						dragBox
							.on('dragover', function (e) {
								e.stopPropagation();
								e.preventDefault();
								dragBox.addClass('is-active');
							})
							.on('dragleave', function (e) {
								e.stopPropagation();
								e.preventDefault();
								dragBox.removeClass('is-active');
							})
							.on('drop', function (e) {
								e.stopPropagation();
								e.preventDefault();

								dragBox.hide();
								dragBox.removeClass('is-active');

								var dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer,
									files = dataTransfer.files;

								if (files.length > 0) self.fileUploader.loadFile(files);
							})
						;
					}

					$('#uploadFile').on('change', function (e) {
						if (e.target.files.length > 0) self.fileUploader.loadFile(e.target.files);
					});

				},

				loadFile: function(files){

					var file = files[0],
						$files = $('.popup_installation').find('.files'),
						html, $field, reader;

					html = '<div class="form__field-inner">' +
								'<div class="input input_file">' +
									'<div class="input__progress">' +
										'<div class="input__progress-line"></div>' +
										'<div class="input__progress-stop js-uploaded-file-remove"></div>' +
									'</div>' +
									'<div class="input__name">file</div>' +
									'<div class="input__stop js-uploaded-file-remove"></div>' +
									'<div class="input__remove js-uploaded-file-remove">Удалить</div>' +
								'</div>' +
							'</div>';

					$field = $('<div/>', {
						'class': 'form__field form__field_file w_1 is-progress',
						'html': html,
						'data-id': 'file['+self.fileUploader.count+']'
					});

					if ( file && !/\.(jpg|jpeg|png|gif)$/i.test( file.name ) || file.size > 8000000 ) {

						self.popup.open('.popup_info', '<div class="popup__title">Ошибка</div><div class="popup__text">Файл слишком большой или не подходит</div>');

					} else if (this && window.File && window.FileReader && window.FileList && window.Blob) {

						$field.appendTo($files);

						$('.js-file-noupload-info').hide();

						reader = new FileReader();
						reader.onprogress = handleReaderProgress;
						reader.onload = function(){
							$field.removeClass('is-progress');
							self.fileUploader.files['file['+self.fileUploader.count+']'] = reader.result;
							self.fileUploader.count++
						};

						reader.readAsDataURL(file);

						$field.find('.input__name').text(file.name);

					}



					function handleReaderProgress(e) {
						// console.info(e);
						if (e.lengthComputable) {

							$field.find('.input__progress-line').css({
								width: 100 / e.total * e.loaded
							});

						}
					}
				}



			},

			scrollMenu: {

				onScroll: function(el){
					var scrollPos = $(document).scrollTop();
					$(el).each(function () {
						var $t = $(this), $section = $($t.attr('href'));
						if ($section.offset().top <= scrollPos && $section.offset().top + $section.height() > scrollPos) {
							$(el).removeClass('active');
							$t.addClass('active');
						}
						else{
							$t.removeClass('active');
						}
					});
				},

				init: function(el){

					$(window).on('scroll', function(){
						self.scrollMenu.onScroll(el);
					});

				}
			},

			checkForm: function(form){
				var $el = $(form),
					wrong = false;

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form__field'),
						$mes = $wrap.find('.form__field-message'),
						val = $.trim($t.val()),
						errMes = '',
						rexp = /.+$/igm;

					$wrap.removeClass('error').removeClass('success');
					$mes.html('');

					if ( $t.attr('type') === 'checkbox' && !$t.is(':checked') ) {
						val = false;
					} else if ( /^(#|\.)/.test(type) ){
						if ( val !== $(type).val() || !val ) val = false;
					} else if ( /^(name=)/.test(type) ){
						if ( val !== $('['+type+']').val() || !val ) val = false;
					} else if ( $t.attr('type') === 'radio'){
						var name =  $t.attr('name');
						if ( $('input[name='+name+']:checked').length < 1 ) val = false;
					} else {
						switch (type) {
							case 'number':
								rexp = /^\d+$/i;
								errMes = 'Поле должно содержать только числовые символы';
								break;
							//case 'phone':
							//	rexp = /[\+]\d\s[\(]\d{3}[\)]\s\d{3}\s\d{2}\s\d{2}/i;
							//	break;
							case 'letter':
								rexp = /^[A-zА-яЁё]+$/i;
								break;
							case 'rus':
								rexp = /^[А-яЁё]+$/i;
								break;
							case 'email':
								rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
								errMes = 'Проверьте корректность email';
								break;
							case 'password':
								rexp = /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
								errMes = 'Слишком простой пароль';
								break;
							default:
								rexp = /.+$/igm;
						}
					}


					if ( !rexp.test(val) || val == 'false' || !val ){
						wrong = true;
						$wrap.addClass('error');

						if (!val) errMes = 'Поле не заполнено';

						if ( errMes ){

							$mes.html(errMes);
						}

					} else {
						$wrap.addClass('success');

					}

					function setMessage(text) {

					}

				});


				return !wrong;

			},

			tooltip: {
				init: function () {
					var $tooltip = $('.tooltip');

					if (!$tooltip.size()) {
						$tooltip = $('<div/>', {'class': 'tooltip'}).appendTo('.wrapper');
					}

					$('body').on('mouseover', '[data-tooltip]', function () {
						var $t = $(this);
						var text = $t.attr('data-tooltip');

						if ( $t.closest('.material_text').length ) return false;

						$tooltip.html('<span class="tooltip-inner">' + text + '</span>').stop(true, true).addClass('show');

						var top = $t.offset().top + $t.outerHeight();
						var left = $t.offset().left + ($t.outerWidth() / 2);
						$tooltip.css({top: top, left: left});

					}).on('mouseleave', '[data-tooltip]', function () {
						$tooltip.removeClass('show');
					});


				}
			},

			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup__overlay"></div>'
						+			'<div class="popup__inner">'
						+				'<div class="popup__layout">'
						+					'<div class="popup__close"></div>'
						+					'<div class="popup__content"></div>'
						+				'</div>'
						+			'</div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					var $popup = $(popup);
					if (!$popup.size()){
						$popup = self.popup.create(popup);
					}
					if( html ){
						$popup.find('.popup__content').html(html);
					}
					$('body').addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(mes, callback){
					var html = '<div class="popup__text">' + mes + '</div>'
						+	'<div class="popup__link"><span class="btn btn_green">Ок</span></div>';
					self.popup.open('.popup_info', html);

					$('.popup_info').find('.btn').click(function(){
						self.popup.close($('.popup_info'));
						if ( callback ) callback();
					});
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.remove();
				}
			},

			scrollBar: {

				destroy: function(el){
					if ( $(el).hasClass('inited') ){
						$(el).filter('.inited').removeClass('inited').mCustomScrollbar('destroy');
					}
				},

				update: function(el){
					if ( $(el).hasClass('inited') ){
						$(el).filter('.inited').removeClass('inited').mCustomScrollbar('update');
					}
				},

				init: function(el, options){

					options = options || {};

					options.callbacks = {
						onCreate: function(){
							$(this).addClass('inited');
						}
					};

					$(el).not('.inited').mCustomScrollbar(options);
				}
			},

			slick: {

				init: function () {


					$('.poster__slider').each(function(){

						var $t = $(this);

						if ( !$t.hasClass('is-slicked') ){
							$t
								.addClass('is-slicked')
								.slick({
									dots: true,
									infinite: true,
									speed: 300,
									slidesToShow: 1,
									touchMove: true,
									prevArrow: '<div class="slick-prev slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1226.36,134.866l1.42-1.512-4.86-4.455,4.81-4.457-1.42-1.512-6.45,5.969Z" transform="translate(-1210 -114)"/></svg></div>',
									nextArrow: '<div class="slick-next slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1263.64,134.866l-1.42-1.512,4.86-4.455-4.81-4.457,1.42-1.512,6.45,5.969Z" transform="translate(-1250 -114)"/></svg></div>'
								});
						}

					});


					$('.cards__slider_x1').each(function(){

						var $t = $(this);

						if ( !$t.hasClass('is-slicked') ){
							$t
								.addClass('is-slicked')
								.slick({
									dots: false,
									infinite: true,
									speed: 300,
									slidesToShow: 1,
									touchMove: true,
									prevArrow: '<div class="slick-prev slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1226.36,134.866l1.42-1.512-4.86-4.455,4.81-4.457-1.42-1.512-6.45,5.969Z" transform="translate(-1210 -114)"/></svg></div>',
									nextArrow: '<div class="slick-next slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1263.64,134.866l-1.42-1.512,4.86-4.455-4.81-4.457,1.42-1.512,6.45,5.969Z" transform="translate(-1250 -114)"/></svg></div>'
								});
						}

					});


					$('.cards__slider_x4').each(function(){

						var $t = $(this);

						if ( !$t.hasClass('is-slicked') ){
							$t
								.addClass('is-slicked')
								.slick({
									dots: false,
									infinite: true,
									speed: 300,
									slidesToShow: 4,
									touchMove: false,
									prevArrow: '<div class="slick-prev slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1226.36,134.866l1.42-1.512-4.86-4.455,4.81-4.457-1.42-1.512-6.45,5.969Z" transform="translate(-1210 -114)"/></svg></div>',
									nextArrow: '<div class="slick-next slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1263.64,134.866l-1.42-1.512,4.86-4.455-4.81-4.457,1.42-1.512,6.45,5.969Z" transform="translate(-1250 -114)"/></svg></div>'
								});
						}
					});


					$('.js-product-preview').each(function(){

						var $t = $(this);

						if ( !$t.hasClass('is-slicked') ){
							$t
								.addClass('is-slicked')
								.slick({
									dots: false,
									infinite: false,
									speed: 300,
									slidesToShow: 4,
									touchMove: false,
									prevArrow: '<div class="slick-prev slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1226.36,134.866l1.42-1.512-4.86-4.455,4.81-4.457-1.42-1.512-6.45,5.969Z" transform="translate(-1210 -114)"/></svg></div>',
									nextArrow: '<div class="slick-next slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1263.64,134.866l-1.42-1.512,4.86-4.455-4.81-4.457,1.42-1.512,6.45,5.969Z" transform="translate(-1250 -114)"/></svg></div>'
								});
						}
					});

					$('.cards__slider_x8').each(function(){
						var $t = $(this);
						if ( !$t.hasClass('is-slicked') ){
							$t
								.addClass('is-slicked')
								.slick({
									dots: false,
									rows: 2,
									infinite: true,
									speed: 300,
									slidesToShow: 4,
									touchMove: false,
									prevArrow: '<div class="slick-prev slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1226.36,134.866l1.42-1.512-4.86-4.455,4.81-4.457-1.42-1.512-6.45,5.969Z" transform="translate(-1210 -114)"/></svg></div>',
									nextArrow: '<div class="slick-next slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path fill="#1a1a1a" d="M1263.64,134.866l-1.42-1.512,4.86-4.455-4.81-4.457,1.42-1.512,6.45,5.969Z" transform="translate(-1250 -114)"/></svg></div>'
								});
						}
					});

				}
			},

			shadow: {

				cost: function($el){

					var $container = $el.find('.cost__container'),
						$wrap = $('.wrapper'),
						$win = $(window),
						$doc = $(document),
						legendAvailability = false,
						legendAbsence = false;

					if ( $el.hasClass('is-disable') || $el.hasClass('f-active') || !$container.length ) return false;

					$el.addClass('f-active');
					self.shadow.show();

					var $btn = $('<div/>', {class:'btn', 'data-focus': 'close', text:'ГОТОВО'});
					var $list = $('<div/>', {class:'cost__list'});

					$list.appendTo($wrap);
					$btn.appendTo($list);



					$container.find('input').each(function(){
						var $input = $(this),
							type = $input.attr('type'),
							preview = $input.data('preview'),
							isDisabled,// $input[0].attributes.disabled,
							isChecked,//$input[0].attributes.checked;
							label = $input.data('label');//$input[0].attributes.checked;
						var $label = $('<div/>', {class:'cost__list-item', text: $input.val()});

						if ( $input[0].attributes.checked ){
							$input[0].removeAttribute('checked');
							$input.prop('checked', true);
						}

						isDisabled = $input.prop('disabled');
						isChecked = $input.prop('checked');

						if ( isDisabled ) legendAbsence = true;
						if ( (/\(\*\)/).test($input.val()) ) legendAvailability = true;


						if ( isChecked ) $label.addClass('is-active');
						if ( isDisabled ) $label.addClass('is-disabled');
						if ( preview ){
							$label.prepend('<div class="cost__list-material" style="background-image:url(' + preview + ')"></div>');
						}
						if ( label ){
							var labelName;

							switch (label){
								case 'hit':
									labelName = 'ХИТ ПРОДАЖ';
									break;
								case 'now':
									labelName = 'НОВИНКА';
									break;
							}

							$label.prepend('<div class="cost__list-label cost__list-label_' + label + '">' + labelName + '</div>');
						}

						$label.on('click', function(){

							if ( isDisabled ) {
								return false;
							} else if ( type === 'radio' && $label.hasClass('is-active') ){
								return false;
							} else if ( type === 'radio' && !$label.hasClass('is-active') ){

								$input.prop('checked', true)
									.trigger('change')
									.siblings()
									.prop('checked', false)
									.trigger('change');
								$label.addClass('is-active').siblings().removeClass('is-active');

							} else if ($label.hasClass('is-active')) {
								$input.prop('checked', false)
									.trigger('change');
								$label.removeClass('is-active');
							} else {
								$input.prop('checked', true)
									.trigger('change');
								$label.addClass('is-active');
							}

						});

						$label.appendTo($list);

					});




					var $legend = '<div class="cost__list-legend">';

					if ( legendAvailability ){
						$legend += '<div class="cost__list-legend-item cost__list-legend-item_1"><i>(*)</i> Наличие уточняйте у менеджера</div>';
					}
					if ( legendAbsence ){
						$legend += '<div class="cost__list-legend-item cost__list-legend-item_2"><i></i> Временно отсутствует</div>';
					}

					$legend += '</div>';

					$list.append($legend);

					setPosition();
					$win.resize(setPosition);


					function setPosition(){

						$btn.css({
							top: - cost().height - 20,
							left: cost().width + 10
						});

						$list.css({
							top: cost().top + cost().height + 20,
							left: cost().left,
							width: cost().winWidth - cost().left - 20
						});
					}

					function cost(){
						return {
							top: $el.offset().top,
							left: $el.offset().left,
							width: $el.outerWidth(true),
							height: $el.outerHeight(true),
							winWidth: $doc.width()
						}
					}


				},

				hide: function(){
					$('.f-active').removeClass('f-active');
					$('body').removeClass('f-shadow');
					$('.cost__list').remove();
				},

				show: function(el){
					$(el).addClass('f-active');
					$('body').addClass('f-shadow');
				},

				init: function () {

					var $b = $('body');

					$b
						.on('click', '.cost__field-clear', function (e) {
							e.preventDefault();
							e.stopPropagation();

							var $t = $(this),
								$wrap = $t.closest('.is-multiple'),
								$field = $wrap.find('.cost__field-val');

							$wrap.removeClass('is-multiple').addClass('is-clean');
							$wrap.find('.c-length').remove();
							$wrap.find('.cost__container')
								.find('input')
								.prop('checked', false);

							$('.cost__list-item').removeClass('is-active');
							if ( !$wrap.find('.c-length').length ){
								$wrap.prepend('<div class="c-length"></div>');
							}
							$wrap.find('.c-length').text('0');
							$field.text('Выбрано 0');
						})

						.on('mousedown touchstart', '.cost__field_counter input', function (e) {
							e.preventDefault();
							return false;
						})

						.on('click', '.cost__field_counter .cost__field-handle', function (e) {
							e.preventDefault();
							var $t = $(this),
								$wrap = $t.closest('.cost__field'),
								$input = $wrap.find('input'),
								val = $input.val();

							if ( $t.hasClass('cost__field-handle_dekr') ){

								if ( val <= 1 ) {
									val = 1;
									return false;
								}

								--val;
							} else if ( $t.hasClass('cost__field-handle_inkr') ){
								++val;
							}
							$input.val(val);

						})

						.on('click', '[data-focus]', function (e) {
							var $this = $(this),
								val = $this.data('focus'),
								el = $this.closest(val)[0]

							;

							switch (val){
								case 'close':
									e.preventDefault();
									self.shadow.hide();
									break;

								case 'select':
									self.shadow.cost($this);
									break;

								default:
									e.preventDefault();
									self.shadow.show(el);
									break;
							}

						})

						.on('change keyup input click', '.cost__field_input input', function() {
							if ( $(this).val().match(/[^0-9]/g) ) {
								var _newVal = $(this).val().replace(/[^0-9]/g, '');
								$(this).val(_newVal);
							}
						})

						.on('change', '.cost__container input', function () {
							var $this = $(this),
								$wrap = $this.closest('.cost__field'),
								$container = $wrap.find('.cost__container'),
								$field = $wrap.find('.cost__field-val'),
								$input = $container.find('input:checked');


							if ( $input.length === 1 ){
								$field.text($input.val());
								$wrap.removeClass('is-multiple');
								$wrap.removeClass('is-clean');
								$wrap.find('.c-length').remove();
							} else {
								$field.text('Выбрано ' + $input.length);
								if ( !$wrap.find('.c-length').length ){
									$wrap.prepend('<div class="c-length"></div>');
								}
								$wrap.find('.c-length').text($input.length);

								if (  $input.length > 1 ){
									$wrap.addClass('is-multiple').removeClass('is-clean');
								} else {
									$wrap.removeClass('is-multiple').addClass('is-clean');
								}

							}

						})

				}
			},

			gallery: {

				initEvent: function ($popup){
					$popup.find('.gallery__preview-slider').each(function () {
						var $t = $(this);

						$t.find('.gallery__preview-list')
							.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
								var $current = $(this).find('[data-slick-index=' + nextSlide + ']'),
									text = $current.attr('data-description'),
									src = $current.find('img').attr('src'),
									$text = $(this).closest('.gallery').find('.gallery__info-description-text .text'),
									$download = $(this).closest('.gallery').find('.gallery__info-description-download'),
									mat = self.gallery.material($current.attr('data-material'), $current.attr('data-bunch'));

								$text.html(text);
								$download.attr('href', src);
								self.gallery.current = nextSlide;
								$popup.find('.gallery__info-material').html(mat);

								if (!!text){
									$popup.find('.gallery__info-description-about').removeClass('is-hide');
								} else {
									$popup.find('.gallery__info-description-about').addClass('is-hide');
								}

								//-console.info($popup.find('.gallery__info-description-text').outerHeight(true));

								$popup.find('.gallery__info-description-about').animate({
									'height': $popup.find('.gallery__info-description-text').outerHeight(true)
								}, 300);
							})
							.on('init', function (slick) {
								if ( self.gallery.current !== 0 ){
									setTimeout(function () {
										$t.find('.gallery__preview-list').slick('slickGoTo', self.gallery.current);
									}, 50);
								}
							})
							.slick({
								slidesToShow: 1,
								slidesToScroll: 1,
								fade: true,
								asNavFor: $popup.find('.gallery__carousel-list'),
								prevArrow: $t.find('.gallery__preview-control-arrow_prev'),
								nextArrow: $t.find('.gallery__preview-control-arrow_next')
							});

					});

					$popup.find('.gallery__carousel-slider').each(function () {
						var $t = $(this);

						$t.find('.gallery__carousel-list').slick({
							dots: false,
							infinite: true,
							touchMove: false,
							speed: 300,
							slidesToShow: 10,
							variableWidth: true,
							centerMode: true,
							asNavFor: $popup.find('.gallery__preview-list'),
							focusOnSelect: true,
							prevArrow: $popup.find('.gallery__carousel-control-arrow_prev'),
							nextArrow: $popup.find('.gallery__carousel-control-arrow_next')
						});
					});

					self.scrollBar.init('.gallery__info-description-about');
				},


				current: 0,

				material: function (a, b) {

					if (!a) return '';

					var j = 0, bunch = b,
						mat = '<div class="material material_notext">' +
								'<div class="material__inner f-product-material">' +
									'<div class="material__head">' +
										'<div class="material__view">' +
											'<div class="material__title">Другие цвета</div><br>' +
												'<div class="material__view-item is-active">Цвет + название</div>' +
													'<div class="material__view-item">Цвет</div>' +
													'</div>' +
												'</div>' +
												'<div class="material__list">',
						arr;

					if ($.isArray(a)){
						arr = a;
					} else {
						try {
							arr = JSON.parse(a.replace(/'/g, '"'));
						} catch (e){
							console.error('json is incorrect');
							return '';
						}
					}

					for(j; j < arr.length; j++){
						mat += '<a href="#" data-tooltip="' + arr[j].color + '" data-bunch="' + arr[j].bunch +'" class="material__item ' + ((bunch && bunch) === arr[j].bunch ? ' is-active' : '' ) + '">' +
							'<div style="background-image:url(' + arr[j].preview + ')" class="material__item-img"></div>' +
							'<div class="material__item-name">' + arr[j].color + '</div>' +
							'</a>';

					}

					mat += '<div data-focus=".f-product-material" class="btn btn_more"></div>' +
											'<div class="material__btn">' +
										'<div data-focus="close" class="btn">ГОТОВО</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>';

					return mat;
				},

				toHTML: function (arr) {
					var i = 0, gal1 = '', gal2 = '', mat = self.gallery.material(arr[0].material, arr[0].bunch);

					for(i; i < arr.length; i++){
						if (!!arr[i].current) self.gallery.current = i;
						gal1 += '<div class="gallery__preview-item" data-bunch="' + arr[i].bunch + '" data-description="' + (arr[i].text || '') + '" data-material="' + (arr[i].material && arr[i].material.length ? JSON.stringify(arr[i].material).replace(/"/g, "\'") : '') + '"><div class="gallery__preview-img"><img data-lazy="' + arr[i].img + '"></div></div>';
						gal2 += '<div class="gallery__carousel-item"><div class="gallery__carousel-img"><img src="' + arr[i].preview + '"></div></div>';
					}


					return '<div class="gallery">' +
							'<div class="gallery__main clearfix">' +
								'<div class="gallery__preview">' +
									'<div class="gallery__preview-slider">' +
										'<div class="gallery__preview-control">' +
											'<div class="gallery__preview-control-arrow gallery__preview-control-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewbox="0 0 30 30"><path fill="#1a1a1a" d="M1226.36,134.866l1.42-1.512-4.86-4.455,4.81-4.457-1.42-1.512-6.45,5.969Z" transform="translate(-1210 -114)"></path></svg></div>' +
											'<div class="gallery__preview-control-arrow gallery__preview-control-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewbox="0 0 30 30"><path fill="#1a1a1a" d="M1263.64,134.866l-1.42-1.512,4.86-4.455-4.81-4.457,1.42-1.512,6.45,5.969Z" transform="translate(-1250 -114)"></path></svg></div>' +
										'</div>' +
										'<div class="gallery__preview-list">' +
											gal1 +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div class="gallery__info">' +
									'<div class="gallery__info-material">' +
										mat +
									'</div>' +
									'<div class="gallery__info-description">' +
										'<div class="gallery__info-description-about ' + (!arr[0].text ? 'is-hide' : '') + '">' +
											'<div class="gallery__info-description-text">' +
												'<div class="title">О товаре</div>' +
												'<div class="text">' + arr[0].text + '</div>' +
											'</div>' +
										'</div>' +
										'<div class="gallery__info-description-btns">' +
											'<a href="' + arr[0].img + '" download class="btn gallery__info-description-download"></a>' +
											'<a class="btn gallery__info-description-share"></a>' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div class="gallery__carousel">' +
									'<div class="gallery__carousel-slider">' +
										'<div class="gallery__carousel-control">' +
											'<div class="gallery__carousel-control-arrow gallery__carousel-control-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewbox="0 0 30 30"><path fill="#1a1a1a" d="M1226.36,134.866l1.42-1.512-4.86-4.455,4.81-4.457-1.42-1.512-6.45,5.969Z" transform="translate(-1210 -114)"></path></svg></div>' +
											'<div class="gallery__carousel-control-arrow gallery__carousel-control-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewbox="0 0 30 30"><path fill="#1a1a1a" d="M1263.64,134.866l-1.42-1.512,4.86-4.455-4.81-4.457,1.42-1.512,6.45,5.969Z" transform="translate(-1250 -114)"></path></svg></div>' +
										'</div>' +
										'<div class="gallery__carousel-list">' +
											gal2 +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>';
				},

				open: function (url) {

					var $popup = self.popup.open('.popup_gallery'),
						$content = $popup.find('.popup__content');

					if ( $content.attr('data-id') === url ) return false;
					$content.attr('data-id', url);

					$.ajax({
						url: url,
						method: 'get'
					}).done(function(data){
						if (data.gallery.length){
							$popup.find('.popup__content').html(self.gallery.toHTML(data.gallery));
							setTimeout(function () {
								self.gallery.initEvent($popup);
							}, 100);
						} else {
							self.popup.close($popup);
							alert('Галерея отсутствует');
						}
					}).fail(function(){
						self.popup.close($popup);
						alert('Галерея отсутствует');
					});

				},

				init: function () {

					$('body').on('click', '[data-gallery]', function (e) {
						e.preventDefault();

						var url = $(this).attr('data-gallery');
						self.gallery.open(url);

					}).on('click', '.popup_gallery .material__item', function (e) {
						e.preventDefault();
						var $t = $(this), $slider = $('.gallery__preview-list'),
						slide = $('.gallery__preview-item[data-bunch=' + $t.attr('data-bunch') + ']').attr('data-slick-index');

						$slider.slick('slickGoTo', slide);

					});

				}
			}

		},
		init: function() {

			self.fileUploader.init();
			self.setMenuPosition.init('.aside');
			self.shadow.init();
			self.tooltip.init();
			self.slick.init();
			self.scrollBar.init('.js-scroll-wrap');

			self.gallery.init();

			$('[data-required=phone]').mask('+7 (999) 999-99-99');


			var $b = $('body');

			$b
				.on('click', '.material__view-item', function (e) {
					e.preventDefault();
					var $this = $(this);

					if (!$this.hasClass('is-active')) {
						$this.addClass('is-active').siblings().removeClass('is-active');
						$this.closest('.material').toggleClass('material_text material_notext');
					}

				})
				.on('click', '.scrollto', function(e){
					e.preventDefault();
					var $this = $(this.hash);
					$('html,body').animate({scrollTop:$this.offset().top}, 500);
				})
				.on('click', '[data-popup]', function(e){
					e.preventDefault();
					e.stopPropagation();
					self.popup.close('.popup');
					self.popup.open($(this).data('popup'));
				})
				// popup close
				/*.on('click', '.popup', function(e){
					if ( !$(e.target).closest('.popup__layout').size() ) self.popup.close('.popup');
				})*/
				.on('click', '.popup__close', function(e){
					e.preventDefault();
					self.popup.close($(this).closest('.popup'));
				})
				// tabs
				.on('click', '[data-tab-link]', function(e){
					/**
					 * data-tab-link='name_1', data-tab-group='names'
					 * data-tab-targ='name_1', data-tab-group='names'
					 **/
					e.preventDefault();
					var $t = $(this),
						group = $t.data('tab-group'),
						$links = $('[data-tab-link]').filter(selectGroup),
						$tabs = $('[data-tab-targ]').filter(selectGroup),
						ind = $t.data('tab-link'),
						$tabItem = $('[data-tab-targ=' + ind + ']').filter(selectGroup),
						time = 150;

					if (!$t.hasClass('is-active')) {
						$links.removeClass('is-active');
						$t.addClass('is-active');
						$tabs.removeClass('is-active');
						$tabItem.addClass('is-active');
					}

					function selectGroup(){
						return $(this).data('tab-group') === group;
					}

				})

				.on('keydown change blur', '.form__field input, .form__field textarea', function () {
					var $t = $(this);

					if ( $.trim($t.val()) ){
						$t.closest('.form__field').addClass('filled');
					} else {
						$t.closest('.form__field').removeClass('filled');
					}

				})

				.on('click', '.js-order-add-item', function (e) {
					e.preventDefault();
					var $t = $(this),
						$order = $('.order__add');

					if ( $t.hasClass('is-loading') ) return false;
						$t.addClass('is-loading');

					if ( $order.hasClass('hidden') ){
						$order.slideDown(200, function () {
							$order.removeClass('hidden').removeAttr('style');
							$t.removeClass('is-loading');
						});
					} else {
						$order.addClass('lightning');
						setTimeout(function () {
							$t.removeClass('is-loading');
							$order.removeClass('lightning');
						}, 400)
					}
				})



			;



			$('.js-search').each(function(){

				var $t = $(this);

				new autoComplete({
					selector: $t[0],
					minChars: 2,
					source: function(val, suggest){
						val = val.toLowerCase();

						$.ajax({
							type: $t.data('method'),
							url: $t.data('action'),
							data: $t.serialize(),
							complete: function (res) {
								var choices = JSON.parse(res.responseText);
								var matches = [];
								for (var i=0; i<choices.length; i++)
									if (~choices[i].toLowerCase().indexOf(val)) matches.push(choices[i]);
								suggest(matches);


							}
						});

					}
				});

			});



			$b
				.on('focus', '.searcher .js-search', function(){
					self.shadow.show('.f-search-result');
				})
				.on('click', '.header__search-exemple span', function(){

					$(this).closest('.search').find('.js-search').val($(this).text());
				})


			;




		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();

